//
//  AddMonsterCategoryVC.swift
//  Gloomhaven Companion
//
//  Created by Steven McGrath on 9/19/18.
//  Copyright © 2018 Steven McGrath. All rights reserved.
//

import UIKit

class AddMonsterCategoryVC: UIViewController {


    // MARK: IBOutlets
    @IBOutlet weak var nameTextField: UITextField!
    // NORMAL
    @IBOutlet weak var normalHpValueLabel: UILabel!
    @IBOutlet weak var normalMovementValueLabel: UILabel!
    @IBOutlet weak var normalAttackValueLabel: UILabel!
    @IBOutlet weak var normalRangeValueLabel: UILabel!
    // ELITE
    @IBOutlet weak var eliteHpValueLabel: UILabel!
    @IBOutlet weak var eliteMovementValueLabel: UILabel!
    @IBOutlet weak var eliteAttackValueLabel: UILabel!
    @IBOutlet weak var eliteRangeValueLabel: UILabel!

    // IBActions
    // NORMAL
    @IBAction func reduceNormalHp(_ sender: UIButton) {
        adjustStatValue(forStatLabel: normalHpValueLabel, by: -1)
    }

    @IBAction func increaseNormalHp(_ sender: UIButton) {
        adjustStatValue(forStatLabel: normalHpValueLabel, by: 1)
    }

    @IBAction func reduceNormalMovement(_ sender: UIButton) {
        adjustStatValue(forStatLabel: normalMovementValueLabel, by: -1)
    }

    @IBAction func increaseNormalMovement(_ sender: UIButton) {
        adjustStatValue(forStatLabel: normalMovementValueLabel, by: 1)
    }

    @IBAction func reduceNormalAttack(_ sender: UIButton) {
        adjustStatValue(forStatLabel: normalAttackValueLabel, by: -1)
    }

    @IBAction func increaseNormalAttack(_ sender: UIButton) {
        adjustStatValue(forStatLabel: normalAttackValueLabel, by: 1)
    }

    @IBAction func reduceNormalRange(_ sender: UIButton) {
        adjustStatValue(forStatLabel: normalRangeValueLabel, by: -1)
    }
    @IBAction func increaseNormalRange(_ sender: UIButton) {
        adjustStatValue(forStatLabel: normalRangeValueLabel, by: 1)
    }

    // ELITE
    @IBAction func reduceEliteHp(_ sender: UIButton) {
        adjustStatValue(forStatLabel: eliteHpValueLabel, by: -1)
    }

    @IBAction func increaseEliteHp(_ sender: UIButton) {
        adjustStatValue(forStatLabel: eliteHpValueLabel, by: 1)
    }

    @IBAction func reduceEliteMovement(_ sender: UIButton) {
        adjustStatValue(forStatLabel: eliteMovementValueLabel, by: -1)
    }

    @IBAction func increaseEliteMovement(_ sender: UIButton) {
        adjustStatValue(forStatLabel: eliteMovementValueLabel, by: 1)
    }

    @IBAction func reduceEliteAttack(_ sender: UIButton) {
        adjustStatValue(forStatLabel: eliteAttackValueLabel, by: -1)
    }

    @IBAction func increaseEliteAttack(_ sender: UIButton) {
        adjustStatValue(forStatLabel: eliteAttackValueLabel, by: 1)
    }

    @IBAction func reduceEliteRange(_ sender: UIButton) {
        adjustStatValue(forStatLabel: eliteRangeValueLabel, by: -1)
    }
    @IBAction func increaseEliteRange(_ sender: UIButton) {
        adjustStatValue(forStatLabel: eliteRangeValueLabel, by: 1)
    }


    @IBAction func createButtonTapped(_ sender: UIButton) {
        guard let categoryName = nameTextField.text else { return }
        print(categoryName)

        guard let normalHp = Int(normalHpValueLabel.text!) else { return }
        guard let normalMovement = Int(normalMovementValueLabel.text!) else { return }
        guard let normalAttack = Int(normalAttackValueLabel.text!) else { return }
        guard let normalRange = Int(normalRangeValueLabel.text!) else { return }
        let normalStats = ["hp": normalHp, "movement": normalMovement, "attack": normalAttack, "range": normalRange]

        guard let eliteHp = Int(eliteHpValueLabel.text!) else { return }
        guard let eliteMovement = Int(eliteMovementValueLabel.text!) else { return }
        guard let eliteAttack = Int(eliteAttackValueLabel.text!) else { return }
        guard let eliteRange = Int(eliteRangeValueLabel.text!) else { return }
        let eliteStats = ["hp": eliteHp, "movement": eliteMovement, "attack": eliteAttack, "range": eliteRange]

        DataService.instance.createMonsterCategory(withName: categoryName, andNormalStats: normalStats, andEliteStats: eliteStats) { (success) in
            if success {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "categoryCreated"), object: nil)
                self.dismiss(animated: true, completion: nil)
            }
        }
    }

    // MARK: Helper Functions
    func adjustStatValue(forStatLabel statLabel: UILabel, by value: Int) {
        statLabel.text = String(Int(statLabel.text!)! + value)
    }

    // MARK: Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}
