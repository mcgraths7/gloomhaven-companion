//
//  StartVC.swift
//  Gloomhaven Companion
//
//  Created by Steven McGrath on 9/22/18.
//  Copyright © 2018 Steven McGrath. All rights reserved.
//

import UIKit

class StartVC: UIViewController {

    // MARK: IBActions
    @IBAction func startButtonTapped(_ sender: UIButton) {
        NotificationCenter.default.post(name: NSNotification.Name("initialize"), object: nil)
    }

    // MARK: Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}
