//
//  MonsterDetailVC.swift
//  Gloomhaven Companion
//
//  Created by Steven McGrath on 9/22/18.
//  Copyright © 2018 Steven McGrath. All rights reserved.
//

import UIKit

class MonsterDetailVC: UIViewController {

    // MARK: IBOutlets
    @IBOutlet weak var monsterLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!

    @IBOutlet weak var currentHpLabel: UILabel!
    @IBOutlet weak var maxHpLabel: UILabel!
    @IBOutlet weak var decreaseHpButton: UIButton!
    @IBOutlet weak var increaseHpButton: UIButton!

    // MARK: IBActions
    @IBAction func decreaseHpButtonTapped(_ sender: UIButton) {
        guard let currentHp = currentHpLabel.text else { return }
        if Int(currentHp) != 0 {
            currentHpLabel.text = String(Int(currentHp)! - 1)
        }
    }
    @IBAction func increaseHpButtonTapped(_ sender: Any) {
        guard let maxHp = maxHpLabel.text else { return }
        guard let currentHp = currentHpLabel.text else { return }
        if Int(currentHp)! < Int(maxHp)! {
            currentHpLabel.text = String(Int(currentHp)! + 1)
        }
    }

    @IBAction func saveChangesButtonTapped(_ sender: Any) {
        guard let currentHp = currentHpLabel.text else { return }
        guard let uid = DataService.instance.selectedMonster?.uid else { return }
        guard let conditions = DataService.instance.selectedMonster?.conditions else { return }
        DataService.instance.updateMonster(forUid: uid, currentHp: Int(currentHp)!, conditions: conditions) { (success) in
            if success {
                self.dismiss(animated: true, completion: nil)
                NotificationCenter.default.post(name: NSNotification.Name("monsterSaved"), object: nil)
            }
        }
    }

    // MARK: Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        loadData()
    }

    // MARK: Observer Functions
    @objc func loadData() {
        guard let category = DataService.instance.selectedMonster?.category else { return }
        guard let currentHp = DataService.instance.selectedMonster?.currentHp else { return }
        guard let maxHp = DataService.instance.selectedMonster?.maxHp else { return }
        monsterLabel.text = category
        currentHpLabel.text = String(currentHp)
        maxHpLabel.text = String(maxHp)
    }
}

// MARK: Table View Setup
extension MonsterDetailVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ConditionCell") as? ConditionCell else { return UITableViewCell() }
        cell.configureCell(withConditionText: DataService.instance.CONDITIONS[indexPath.row])
        if let monster = DataService.instance.selectedMonster {
            if monster.conditions[DataService.instance.CONDITIONS[indexPath.row]] == true {
                tableView.selectRow(at: indexPath, animated: false, scrollPosition: .none)
            } else {
                tableView.deselectRow(at: indexPath, animated: false)
            }
        }

        return cell
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let cell = tableView.cellForRow(at: indexPath) as? ConditionCell else { return }
        guard let condition = cell.conditionLabel.text else { return }
        DataService.instance.selectedMonster?.conditions[condition] = true
    }

    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        guard let cell = tableView.cellForRow(at: indexPath) as? ConditionCell else { return }
        guard let condition = cell.conditionLabel.text else { return }
        DataService.instance.selectedMonster?.conditions[condition] = false
    }
}
