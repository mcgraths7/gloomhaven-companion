//
//  CategoriesVC.swift
//  Gloomhaven Companion
//
//  Created by Steven McGrath on 9/19/18.
//  Copyright © 2018 Steven McGrath. All rights reserved.
//

import UIKit

class CategoriesVC: UIViewController {

    var monsterCategories = [MonsterCategory]()
    var monsterCounts: [String: Int] = [:]

    // MARK: IBOutlets
    @IBOutlet weak var tableView: UITableView!

    // MARK: Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        NotificationCenter.default.addObserver(self, selector: #selector(loadData), name: NSNotification.Name("monsterCreated"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(loadData), name: NSNotification.Name("categoryCreated"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(loadData), name: NSNotification.Name("initialize"), object: nil)
    }

    // MARK: Observer Functions
    @objc func loadData() {
        DataService.instance.getAllMonsterCategories { (returnedCategoriesArray) in
            self.monsterCategories = returnedCategoriesArray
            for category in returnedCategoriesArray {
                DataService.instance.getAllMonsters(forCategory: category, completion: { (returnedMonstersArray) in
                    if let categoryName = category.name {
                        self.monsterCounts[categoryName] = returnedMonstersArray.count
                        self.tableView.reloadData()
                    }
                })
            }
        }
    }
}

// MARK: Table view setup
extension CategoriesVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return monsterCategories.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryCell") as? CategoryCell else { return UITableViewCell() }
        let category = monsterCategories[indexPath.row]
        guard let categoryName = category.name else { return UITableViewCell() }
        if let currentUnits = monsterCounts[categoryName] {
            cell.configureCell(withCategoryName: category.name, andCurrentUnits: currentUnits, andMaxUnits: 10)
            return cell
        }
        cell.configureCell(withCategoryName: category.name, andCurrentUnits: 0, andMaxUnits: 10)
        return cell
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let cell = tableView.cellForRow(at: indexPath) as? CategoryCell else { return }
        guard let categoryName = cell.categoryNameLabel.text else { return }
        DataService.instance.getAllMonsterCategories { (returnedCategoryArray) in
            for category in returnedCategoryArray {
                if category.name == categoryName {
                    DataService.instance.selectedCategory = categoryName
                    self.performSegue(withIdentifier: "toMonsters", sender: self)
                    self.tableView.deselectRow(at: indexPath, animated: false)
                }
            }
        }
    }
}
