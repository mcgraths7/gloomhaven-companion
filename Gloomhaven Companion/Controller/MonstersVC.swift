//
//  AddMonsterVC.swift
//  Gloomhaven Companion
//
//  Created by Steven McGrath on 9/20/18.
//  Copyright © 2018 Steven McGrath. All rights reserved.
//

import UIKit

class MonstersVC: UIViewController {

    // MARK: Table Variables
    var monsters = [Monster]()

    // MARK: IBOutlets
    @IBOutlet weak var tableView: UITableView!

    @IBOutlet weak var eliteButton: UIButton!
    @IBOutlet weak var normalButton: UIButton!
    @IBOutlet weak var monsterCategoryLabel: UILabel!

    // MARK: IBActions
    @IBAction func backButtonTapped(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }

    @IBAction func addNormalButtonTapped(_ sender: UIButton) {
        DataService.instance.createMonster(fromCategory: DataService.instance.selectedCategory, andEliteStatus: false) { (success) in
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "monsterCreated"), object: nil)
        }
    }
    @IBAction func addEliteButtonTapped(_ sender: UIButton) {
        DataService.instance.createMonster(fromCategory: DataService.instance.selectedCategory, andEliteStatus: true) { (success) in
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "monsterCreated"), object: nil)
        }
    }

    // MARK: Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        loadData()
        NotificationCenter.default.addObserver(self, selector: #selector(loadData), name: NSNotification.Name("monsterSaved"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(loadData), name: NSNotification.Name("monsterCreated"), object: nil)
    }

    // MARK: Observer Methods
    @objc func loadData() {
        let categoryText = DataService.instance.selectedCategory
        if categoryText.last! == "s" {
            monsterCategoryLabel.text = "\(categoryText)es"
        } else {
            monsterCategoryLabel.text = "\(categoryText)s"
        }
        if monsters.count > 9 {
            eliteButton.isEnabled = false
            normalButton.isEnabled = false
        } else {
            eliteButton.isEnabled = true
            normalButton.isEnabled = true
        }
        DataService.instance.getAllMonsters(completion: { (returnedMonsterArray) in
            let filteredArray = returnedMonsterArray.filter( { $0.category == DataService.instance.selectedCategory })
            self.monsters = filteredArray
            self.tableView.reloadData()
        })
    }

}
// MARK: TableView Setup
extension MonstersVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return monsters.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "MonsterCell") as? MonsterCell else { return MonsterCell() }
        let monster = monsters[indexPath.row]
        DataService.instance.getAllMonsterCategories { (returnedCategoriesArray) in
            for category in returnedCategoriesArray {
                if category.name == DataService.instance.selectedCategory {
                    let hpString = "\(monster.currentHp) / \(monster.maxHp)"
                    cell.configureCell(withCategoryName: DataService.instance.selectedCategory, andHpString: hpString)
                }
            }
        }
        return cell
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        DataService.instance.selectedMonster = monsters[indexPath.row]
        performSegue(withIdentifier: "toMonsterDetail", sender: nil)
    }
}
