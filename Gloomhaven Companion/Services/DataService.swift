//
//  DataService.swift
//  Gloomhaven Companion
//
//  Created by Steven McGrath on 9/19/18.
//  Copyright © 2018 Steven McGrath. All rights reserved.
//

import Foundation
import Firebase

class DataService {
    static let instance = DataService()

    private var _REF_BASE = DB_BASE
    private var _REF_MONSTER_CATEGORIES = DB_BASE.child("monster-categories")
    private var _REF_MONSTERS = DB_BASE.child("monsters")

    var REF_BASE: DatabaseReference {
        return _REF_BASE
    }

    var REF_MONSTER_CATEGORIES: DatabaseReference {
        return _REF_MONSTER_CATEGORIES
    }

    var REF_MONSTERS: DatabaseReference {
        return _REF_MONSTERS
    }

    var selectedCategory: String = ""
    var selectedMonster: Monster?

    let CONDITIONS = ["poison", "wound", "immobilize", "disarm", "stun", "muddle", "curse", "invisible", "strengthen", "bless"]

    func createMonsterCategory(withName name: String, andNormalStats normalStats: [String: Int], andEliteStats eliteStats: [String: Int], completion: @escaping (_ status: Bool) -> ()) {
        REF_MONSTER_CATEGORIES.childByAutoId().updateChildValues(["name": name, "normal-stats": normalStats, "elite-stats": eliteStats, "max-units": 10, "current-units": 0])
        completion(true)
    }

    func getAllMonsterCategories(completion: @escaping (_ monsterCategories: [MonsterCategory]) -> ()) {
        REF_MONSTER_CATEGORIES.observeSingleEvent(of: .value) { (returnedMonsterCategoriesSnapshot) in
            var monsterCategoryArray = [MonsterCategory]()
            guard let snapshot = returnedMonsterCategoriesSnapshot.children.allObjects as? [DataSnapshot] else { return }
            for category in snapshot {
                let name = category.childSnapshot(forPath: "name").value as! String
                let normalStats = category.childSnapshot(forPath: "normal-stats").value as! [String: Int]
                let eliteStats = category.childSnapshot(forPath: "elite-stats").value as! [String: Int]
                let newCategory = MonsterCategory(name: name, normalStats: normalStats, eliteStats: eliteStats)
                monsterCategoryArray.append(newCategory)
            }
            completion(monsterCategoryArray)
        }
    }

    func createMonster(fromCategory categoryName: String, andEliteStatus elite: Bool, completion: @escaping (_ status: Bool) -> ()) {
        getAllMonsterCategories { (returnedMonsterCategoryArray) in
            let filteredArray = returnedMonsterCategoryArray.filter( { $0.name == categoryName } )
            if filteredArray.isEmpty {
                completion(false)
            } else {
                let conditions: [String: Bool] = ["poison": false, "wound": false, "immobilize": false, "disarm": false, "stun": false, "muddle": false, "curse": false, "invisible": false, "strengthen": false, "bless": false]
                let selectedCategory = filteredArray.first
                if elite == true {
                    guard let hp = selectedCategory?.eliteStats["hp"] else { return }
                    self.REF_MONSTERS.childByAutoId().updateChildValues(["category": categoryName, "maxHp": hp, "currentHp": hp, "conditions": conditions, "elite": elite])
                } else {
                    guard let hp = selectedCategory?.normalStats["hp"] else { return }
                    self.REF_MONSTERS.childByAutoId().updateChildValues(["category": categoryName, "maxHp": hp, "currentHp": hp, "conditions": conditions, "elite": elite])
                }
                completion(true)
            }
        }
    }

    func getAllMonsters(completion: @escaping (_ monsters: [Monster]) -> ()) {
        REF_MONSTERS.observeSingleEvent(of: .value) { (returnedMonsterSnapshot) in
            var monsterArray = [Monster]()
            guard let snapshot = returnedMonsterSnapshot.children.allObjects as? [DataSnapshot] else { return }
            for monster in snapshot {
                let uid = monster.key
                let currentHp = monster.childSnapshot(forPath: "currentHp").value as! Int
                let maxHp = monster.childSnapshot(forPath: "maxHp").value as! Int
                let conditions = monster.childSnapshot(forPath: "conditions").value as! [String: Bool]
                let elite = monster.childSnapshot(forPath: "elite").value as! Bool
                let category = monster.childSnapshot(forPath: "category").value as! String
                let newMonster = Monster(forUid: uid, andCategory: category, withMaxHp: maxHp, andCurrentHp: currentHp, withConditions: conditions, isElite: elite)
                monsterArray.append(newMonster)
            }
            completion(monsterArray)
        }
    }

    func getAllMonsters(forCategory monsterCat: MonsterCategory, completion: @escaping (_ monsters: [Monster]) -> ()) {
        REF_MONSTERS.observeSingleEvent(of: .value) { (returnedMonsterSnapshot) in
            var monsterArray = [Monster]()

            guard let snapshot = returnedMonsterSnapshot.children.allObjects as? [DataSnapshot] else { return }

            for monster in snapshot {
                let uid = monster.key
                let currentHp = monster.childSnapshot(forPath: "currentHp").value as! Int
                let maxHp = monster.childSnapshot(forPath: "maxHp").value as! Int
                let conditions = monster.childSnapshot(forPath: "conditions").value as! [String: Bool]
                let elite = monster.childSnapshot(forPath: "elite").value as! Bool
                let category = monster.childSnapshot(forPath: "category").value as! String
                let newMonster = Monster(forUid: uid, andCategory: category, withMaxHp: maxHp, andCurrentHp: currentHp, withConditions: conditions, isElite: elite)
                if category == monsterCat.name {
                    monsterArray.append(newMonster)
                }
            }
            completion(monsterArray)
        }
    }

    func updateMonster(forUid uid: String, currentHp: Int, conditions: [String: Bool], completion: @escaping (_ status: Bool) -> ()) {
        guard let monsterId = DataService.instance.selectedMonster?.uid else { return }
        let monsterRef = REF_MONSTERS.child(monsterId)
        monsterRef.updateChildValues(["currentHp": currentHp, "conditions": conditions])
        completion(true)
    }
}
