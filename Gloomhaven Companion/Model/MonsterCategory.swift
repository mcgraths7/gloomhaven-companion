//
//  MonsterCategory.swift
//  Gloomhaven Companion
//
//  Created by Steven McGrath on 9/19/18.
//  Copyright © 2018 Steven McGrath. All rights reserved.
//

import Foundation

struct MonsterCategory {
    private(set) public var name: String!
    private(set) public var normalStats: [String: Int]
    private(set) public var eliteStats: [String: Int]

    init(name: String, normalStats: [String: Int], eliteStats: [String: Int]) {
        self.name = name
        self.normalStats = normalStats
        self.eliteStats = eliteStats
    }
}
