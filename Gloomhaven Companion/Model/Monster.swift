//
//  Monster.swift
//  Gloomhaven Companion
//
//  Created by Steven McGrath on 9/19/18.
//  Copyright © 2018 Steven McGrath. All rights reserved.
//

import Foundation

struct Monster {
    var uid: String
    var currentHp: Int
    var conditions: [String: Bool]
    var category: String
    var maxHp: Int
    var isElite: Bool

    init(forUid uid: String, andCategory category: String, withMaxHp maxHp: Int, andCurrentHp currentHp: Int, withConditions conditions: [String: Bool], isElite: Bool) {
        self.uid = uid
        self.category = category
        self.maxHp = maxHp
        self.currentHp = currentHp
        self.conditions = conditions
        self.isElite = isElite
    }

    
}
