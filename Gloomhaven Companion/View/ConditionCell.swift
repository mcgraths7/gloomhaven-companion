//
//  ConditionCell.swift
//  Gloomhaven Companion
//
//  Created by Steven McGrath on 9/22/18.
//  Copyright © 2018 Steven McGrath. All rights reserved.
//

import UIKit

class ConditionCell: UITableViewCell {

    @IBOutlet weak var conditionLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func configureCell(withConditionText condition: String) {
        self.conditionLabel.text = condition
    }
}
