//
//  MonsterCell.swift
//  Gloomhaven Companion
//
//  Created by Steven McGrath on 9/22/18.
//  Copyright © 2018 Steven McGrath. All rights reserved.
//

import UIKit

class MonsterCell: UITableViewCell {

    @IBOutlet weak var monsterCategoryLabel: UILabel!
    @IBOutlet weak var hpLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func configureCell(withCategoryName categoryName: String, andHpString hpString: String) {
        monsterCategoryLabel.text = categoryName
        hpLabel.text = hpString
    }

}
