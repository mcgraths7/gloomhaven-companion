//
//  CategoryCell.swift
//  Gloomhaven Companion
//
//  Created by Steven McGrath on 9/19/18.
//  Copyright © 2018 Steven McGrath. All rights reserved.
//

import UIKit

class CategoryCell: UITableViewCell {

    @IBOutlet weak var currentUnitsLabel: UILabel!
    @IBOutlet weak var categoryNameLabel: UILabel!

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    func configureCell(withCategoryName name: String, andCurrentUnits currentUnits: Int, andMaxUnits maxUnits: Int) {
        self.categoryNameLabel.text = name
        self.currentUnitsLabel.text = String(currentUnits)
    }

}
