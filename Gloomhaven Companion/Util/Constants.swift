//
//  Constants.swift
//  Gloomhaven Companion
//
//  Created by Steven McGrath on 9/19/18.
//  Copyright © 2018 Steven McGrath. All rights reserved.
//

import Foundation
import Firebase

let DB_BASE = Database.database().reference()
